import logging
import sys

from alexa import objects, data
from statemachine.exceptions import TransitionNotAllowed

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger('scene')

class Scene:

    room_objects = {}

    def __init__(self, from_dict=None):
        if from_dict is None:
            self.room_objects = {
                objects.KEY : objects.Key(),
                objects.JACKET : objects.Jacket(),
                objects.FILING_CABINET : objects.FilingCabinet(),
                objects.CLOSET : objects.Closet(),
                objects.USB_KEY : objects.UsbKey(),
                objects.WINDOW : objects.Window(),
                objects.COMPUTER: objects.Computer()
            }
            return

        self.room_objects = { id: objects.create_object(id, state) for (id, state) in from_dict.items() }


# Utility code

    def to_dict(self):
        output = {}
        for key in self.room_objects:
            output[key] = self.room_objects[key].current_state.value

        return output

    def get_objects_with_state(self, state):
        return {id: object for (id, object) in self.room_objects.items() if object.current_state.value == state}

    def describe(self, id):
        return data.DESCRIBE[id]

# Higher level functions
    def get_all_visible_objects(self):
        return self.get_objects_with_state(objects.STATE_VISIBLE)

    def can_perform_action(self, object_id, verb):
        object = self.room_objects[object_id]
        verbs = [t.identifier for t in object.transitions]
        return verb in verbs

# Player actions - these return text indicating what should be communicated to the player

    def look_around(self):
        '''
        Gets a description of all visible objects for the player, triggering 'notice'
        transition for anything which can be seen currently
        '''
        for object in self.room_objects.values():
            for transition in object.transitions:
                if transition.identifier == objects.ACTION_NOTICE:
                    log.debug("Trying to notice object '{}'".format(object.id))
                    try:
                        object.notice()
                    except TransitionNotAllowed:
                        log.debug("Not a valid state transition right now...")
                    finally:
                        break

        visible_object_keys = [key for key in self.room_objects.keys() if
                self.room_objects[key].current_state.identifier != objects.STATE_UNNOTICED and
                not self.room_objects[key].current_state.identifier.startswith("in_")]
        last_object_key = visible_object_keys.pop()
        output = "There's a " + ", a ".join([self.describe(i) for i in visible_object_keys])
        output += " and a " + self.describe(last_object_key)
        log.debug("Look around output is '{}'".format(output))
        return output

    def apply_object_verb(self, object_id, verb):
        '''
        Utility method to update the state of an object as the result of another action
        '''
        object = self.room_objects[object_id]
        return object.run(verb)

    def look_at(self, object_id):
        '''
        Look at a specific object and get a more detailed description
        '''
        visible_object_keys = list(self.get_all_visible_objects().keys())
        if object_id in visible_object_keys:
            # @todo consider object state
            log.debug("Look at output is '{}'".format(data.LOOK_AT[object_id]))
            return data.LOOK_AT[object_id]

        output = ". ".join([data.CANT_SEE_THAT, data.WILL_LOOK_AROUND])
        look_around = self.look_around()

        visible_object_keys = list(self.get_all_visible_objects().keys())
        if object_id in visible_object_keys:
            # @todo consider object state
            output = ". ".join([output, look_around, data.FOUND_IT, data.LOOK_AT[object_id]])
            log.debug("Look at output is '{}'".format(output))
            return output

        output = ". ".join([output, look_around, data.DEFINITELY_NOT_THERE])
        log.debug("Look at output is '{}'".format(output))
        return output

    def open(self, object_id):
        '''
        Open an object
        '''
        if self.can_perform_action(object_id, objects.ACTION_OPEN):
            object = self.room_objects[object_id]
            # Check object has been noticed first
            if object.current_state.identifier == objects.STATE_UNNOTICED:
                output = data.OBJECT_UNNOTICED
            else:
                try:
                    object.open()
                    output = data.OPEN_RESPONSE[object_id]
                    # Special cases - opening closet notices jacket
                    if object_id == objects.CLOSET:
                        self.apply_object_verb(objects.JACKET, objects.ACTION_FIND)
                except TransitionNotAllowed:
                    # Opening will fail if already open!
                    output = data.OPEN_RESPONSE[data.ALREADY_KEY]
        else:
            output = data.OPEN_RESPONSE[data.CANT_KEY]

        log.debug("Look at output is '{}'".format(output))
        return output

    def search(self, object_id):
        '''
        Search an object
        '''
        if self.can_perform_action(object_id, objects.ACTION_SEARCH):
            object = self.room_objects[object_id]
            # Check object has been noticed first
            if object.current_state.identifier == objects.STATE_UNNOTICED:
                output = data.OBJECT_UNNOTICED
            else:
                try:
                    object.search()
                    output = data.SEARCH_RESPONSE[object_id]

                    # Find objects
                    if object_id == objects.JACKET:
                        self.apply_object_verb(objects.KEY, objects.ACTION_FIND)

                    # Find objects
                    if object_id == objects.FILING_CABINET:
                        self.apply_object_verb(objects.USB_KEY, objects.ACTION_FIND)


                except TransitionNotAllowed:
                    # Searching will fail if already searched!
                    output = data.SEARCH_RESPONSE[data.ALREADY_KEY]
        else:
            output = data.SEARCH_RESPONSE[data.CANT_KEY]

        log.debug("Search output is '{}'".format(output))
        return output
