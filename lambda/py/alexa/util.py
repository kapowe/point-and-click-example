# -*- coding: utf-8 -*-

"""Gatehack Utility module."""

import random
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def get_resolved_value(request, slot_name):
    """Resolve the slot name from the request."""
    # type: (IntentRequest, str) -> Union[str, None]
    try:
        return request.intent.slots[slot_name].value
    except (AttributeError, ValueError, KeyError, IndexError):
        return None

def game_started(handler_input):
    """Determine whether the user has a game active currently"""
    attr = handler_input.attributes_manager.session_attributes
    return "guess_number" in attr
