
from statemachine import StateMachine, State

COMPUTER = 'computer'
USB_KEY = 'u.s.b key'
FILING_CABINET = 'filing Cabinet'
CLOSET = 'closet'
JACKET = 'jacket'
KEY = 'key'
WINDOW = 'window'

# STATE CONSTANTS
STATE_VISIBLE = 'visible'
STATE_ON = 'on'
STATE_OPENED = 'opened'
STATE_IN_INVENTORY = 'In Inventory'
STATE_UNNOTICED = 'unnoticed'
STATE_FOUND = 'found'

# VERBS
ACTION_NOTICE = 'notice'
ACTION_OPEN = 'open'
ACTION_TAKE = 'take'
ACTION_SEARCH = 'search'
ACTION_FIND = 'find'

def create_object(id, state):
    model = ObjectModel(state)
    if id == COMPUTER:
        return Computer(model)

    if id == USB_KEY:
        return UsbKey(model)

    if id == FILING_CABINET:
        return FilingCabinet(model)

    if id == CLOSET:
        return Closet(model)

    if id == JACKET:
        return Jacket(model)

    if id == KEY:
        return Key(model)

    if id == WINDOW:
        return Window(model)

    raise Exception("Unrecognised object id '{}'".format(id))


class ObjectModel(object):
    def __init__(self, state):
        self.state = state

class Computer(StateMachine):

    id = COMPUTER

    unnoticed = State(STATE_UNNOTICED, initial=True)
    visible = State(STATE_VISIBLE)
    on = State(STATE_ON)
    opened = State(STATE_OPENED)
    file_available = State('File Available')

    notice = unnoticed.to(visible)
    turn_on = visible.to(on)
    turn_off = on.to(visible)
    open = visible.to(opened)
    close = opened.to(visible)

    use_with_usb = on.to(file_available)

class UsbKey(StateMachine):

    id = USB_KEY

    in_filing_cabinet = State('In Filing Cabinet', initial=True)
    visible = State(STATE_VISIBLE)
    taken = State(STATE_IN_INVENTORY)
    in_computer = State('In Computer')

    find = in_filing_cabinet.to(visible)
    take = visible.to(taken)
    use_with_computer = taken.to(in_computer)

class FilingCabinet(StateMachine):

    id = FILING_CABINET

    unnoticed = State(STATE_UNNOTICED, initial=True)
    visible = State(STATE_VISIBLE)
    opened = State(STATE_OPENED)
    searched = State('Searched')

    notice = unnoticed.to(visible)
    open = visible.to(opened)
    search = opened.to(searched)
    push = opened.to(searched)

class Closet(StateMachine):

    id = CLOSET

    unnoticed = State(STATE_UNNOTICED, initial=True)
    visible = State(STATE_VISIBLE)
    opened = State(STATE_OPENED)

    notice = unnoticed.to(visible)
    open = visible.to(opened)

class Jacket(StateMachine):

    id = JACKET

    in_closet = State('In Closet', initial=True)
    found = State(STATE_FOUND)
    searched = State('Searched')

    find = in_closet.to(found)
    search = found.to(searched)

class Key(StateMachine):

    id = KEY

    in_jacket = State('In Jacket', initial=True)
    found = State(STATE_FOUND)
    taken = State(STATE_IN_INVENTORY)
    in_filing_cabinet = State('In Filing Cabinet')

    find = in_jacket.to(found)
    take = found.to(taken)
    use_with_filing_cabinet= taken.to(in_filing_cabinet)

class Window(StateMachine):

    id = WINDOW

    unnoticed = State(STATE_UNNOTICED, initial=True)
    visible = State(STATE_VISIBLE)

    notice = unnoticed.to(visible)
