from alexa import objects

# Standard barks
CANT_SEE_THAT = "I can't see that right now"
WILL_LOOK_AROUND = "Let me look around a little"
CANT_FIND_THAT = "I can't find what you're looking for"
DEFINITELY_NOT_THERE = "I definitely can't see what you're looking for"
FOUND_IT = "Found it"
OBJECT_UNNOTICED = "Where is that? I haven't seen that around here"

# For invalid targets for actions
CANT_KEY = "cant"
ALREADY_KEY = "already_performed"
UNNOTICED_KEY = "unnoticed"

# Descriptions when looking at objects
DESCRIBE = {
    objects.KEY : "small shiny metal key",
    objects.JACKET : "ratty pleather biker jacket",
    objects.FILING_CABINET : "dinted metal filing cabinet",
    objects.CLOSET : "shallow two door closet",
    objects.USB_KEY : "small USB thumb drive",
    objects.WINDOW : "floor to ceiling tinted glass window",
    objects.COMPUTER : "old desktop PC"
}

LOOK_AT = {
    objects.KEY : "It's not a key for a door or anything. It looks like it belongs with a suitcase or some sort of office equipment",
    objects.JACKET : "The sleeves are all tatty and worn, and it smells like sandalwood. There's a couple of pockets on the sleeve that are zipped up",
    objects.FILING_CABINET : "It's a three drawer steel cabinet. It's shut tight right now. There's a small lock at the top of it",
    objects.CLOSET : "It's a pretty unremarkable two door closet. Some light scratches near the handles for the doors",
    objects.USB_KEY : "It's an 8 terabyte Seegson thumb drive. Looks well used",
    objects.WINDOW : "The view from here is pretty incredible. This place must have cost a mint in rent",
    objects.COMPUTER : "It's an old Alphatech. Workhorse model. Looks well used. It's still plugged in and good to go"
}

OPEN_RESPONSE = {
    objects.COMPUTER : "Wires and dust bunnies. There's nothing of interest in here. It's probably more useful closed",
    objects.CLOSET : "Hey, there's an old jacket in here",
    objects.FILING_CABINET : "Huh. It's empty. I felt a bump - I think there's something stuck under one of the drawers",
    CANT_KEY : "I can't open that",
    ALREADY_KEY : "That's already open"
}

SEARCH_RESPONSE = {
    ALREADY_KEY : "I've already searched in there pretty thoroughly",
    CANT_KEY : "I can't search that",
    objects.JACKET : "Hey - there's a key in here. A small one",
    objects.FILING_CABINET : "Hey - there's a small USB drive in here. This might have the file we're looking for"
}
