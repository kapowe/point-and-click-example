# -*- coding: utf-8 -*-

# This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK for Python.
# Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
# session persistence, api calls, and more.
# This sample is built using the handler classes approach in skill builder.
import logging
import ask_sdk_core.utils as ask_utils

from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.dispatch_components import (
    AbstractRequestHandler, AbstractExceptionHandler,
    AbstractResponseInterceptor, AbstractRequestInterceptor)
from ask_sdk_model import Response
from alexa import util, objects, scene, data

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

#Constants
FIRST_OBJECT = 'firstObject'
SECOND_OBJECT = 'secondObject'
ROOM_KEY = 'room'

class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("LaunchRequestHandler START")

        speak_output = "Alright, I'm in the room. What do you want me to do?"
        session_attr = handler_input.attributes_manager.session_attributes
        room = scene.Scene()
        session_attr[ROOM_KEY] = room.to_dict()

        logger.info("LaunchRequestHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )

class LookAtIntentHandler(AbstractRequestHandler):
    """Handler for Hello World Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("LookAtIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("LookAtIntentHandler START")

        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        session_attr = handler_input.attributes_manager.session_attributes
        room = scene.Scene(session_attr[ROOM_KEY])
        speak_output = room.look_at(object)
        session_attr[ROOM_KEY] = room.to_dict()

        logger.info("LookAtIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask("What's next?")
                .response
        )


class TakeIntentHandler(AbstractRequestHandler):
    """Handler for Take Intent - interacting with objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("TakeIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("TakeIntentHandler START")
        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        speak_output = "You want me to take the {}".format(object)
        logger.info("TakeIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class UseIntentHandler(AbstractRequestHandler):
    """Handler for Use Intent - interacting with objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("UseIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("UseIntentHandler START")
        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        withObject = util.get_resolved_value(
            handler_input.request_envelope.request, SECOND_OBJECT)
        speak_output = "You want me to use the {}".format(object)
        if withObject is not None:
            speak_output += " with the {}".format(withObject)
        logger.info("UseIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class LookAroundIntentHandler(AbstractRequestHandler):
    """Handler for Use Intent - interacting with objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("LookAroundIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("LookAroundIntentHandler START")

        session_attr = handler_input.attributes_manager.session_attributes
        room = scene.Scene(session_attr[ROOM_KEY])
        speak_output = room.look_around()
        session_attr[ROOM_KEY] = room.to_dict()

        logger.info("LookAroundIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask("What's next?")
                .response
        )


class SearchIntentHandler(AbstractRequestHandler):
    """Handler for Search Intent - interrogating objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("SearchIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("SearchIntentHandler START")

        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        session_attr = handler_input.attributes_manager.session_attributes
        room = scene.Scene(session_attr[ROOM_KEY])
        speak_output = room.search(object)
        session_attr[ROOM_KEY] = room.to_dict()

        logger.info("SearchIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask("What's next?")
                .response
        )


class PushIntentHandler(AbstractRequestHandler):
    """Handler for Push Intent - shoving objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("PushIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("PushIntentHandler START")
        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        speak_output = "You want me to push the {}".format(object)
        logger.info("PushIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class TurnOnIntentHandler(AbstractRequestHandler):
    """Handler for Turn On Intent - deactivating objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("TurnOnIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("TurnOnIntentHandler START")
        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        speak_output = "You want me to turn on the {}".format(object)
        logger.info("TurnOnIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class TurnOffIntentHandler(AbstractRequestHandler):
    """Handler for Turn Off Intent - deactivating objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("TurnOffIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("TurnOffIntentHandler START")
        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        speak_output = "You want me to turn off the {}".format(object)
        logger.info("TurnOffIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class OpenIntentHandler(AbstractRequestHandler):
    """Handler for Open Intent - opening objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("OpenIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("OpenIntentHandler START")

        session_attr = handler_input.attributes_manager.session_attributes
        room = scene.Scene(session_attr[ROOM_KEY])
        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        speak_output = room.open(object)

        session_attr[ROOM_KEY] = room.to_dict()


        logger.info("OpenIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask("What's next?")
                .response
        )


class CloseIntentHandler(AbstractRequestHandler):
    """Handler for Close Intent - closing objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("CloseIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("CloseIntentHandler START")
        object = util.get_resolved_value(
            handler_input.request_envelope.request, FIRST_OBJECT)
        speak_output = "You want me to open the {}".format(object)
        logger.info("CloseIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class InventoryIntentHandler(AbstractRequestHandler):
    """Handler for Use Intent - interacting with objects"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("InventoryIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("InventoryIntentHandler START")
        speak_output = "You want to know what I'm carrying"
        logger.info("InventoryIntentHandler END")
        return (
            handler_input.response_builder
                .speak(speak_output)
                # .ask("add a reprompt if you want to keep the session open for the user to respond")
                .response
        )


class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "You can say hello to me! How can I help?"

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return (ask_utils.is_intent_name("AMAZON.CancelIntent")(handler_input) or
                ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input))

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speak_output = "Goodbye!"

        return (
            handler_input.response_builder
                .speak(speak_output)
                .response
        )


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response

        # Any cleanup logic goes here.

        return handler_input.response_builder.response


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors. If you receive an error
    stating the request handler chain is not found, you have not implemented a handler for
    the intent being invoked or included it in the skill builder below.
    """
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        speak_output = "Sorry, I had trouble doing what you asked. Please try again."

        return (
            handler_input.response_builder
                .speak(speak_output)
                .ask(speak_output)
                .response
        )

# Request and Response Loggers
class RequestLogger(AbstractRequestInterceptor):
    """Log the request envelope."""
    def process(self, handler_input):
        # type: (HandlerInput) -> None
        logger.info("Request Envelope: {}".format(
            handler_input.request_envelope))


class ResponseLogger(AbstractResponseInterceptor):
    """Log the response envelope."""
    def process(self, handler_input, response):
        # type: (HandlerInput, Response) -> None
        logger.info("Response: {}".format(response))


# The SkillBuilder object acts as the entry point for your skill, routing all request and response
# payloads to the handlers above. Make sure any new handlers or interceptors you've
# defined are included below. The order matters - they're processed top to bottom.

sb = SkillBuilder()

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(LookAtIntentHandler())
sb.add_request_handler(TakeIntentHandler())
sb.add_request_handler(LookAroundIntentHandler())
sb.add_request_handler(UseIntentHandler())
sb.add_request_handler(SearchIntentHandler())
sb.add_request_handler(PushIntentHandler())
sb.add_request_handler(TurnOnIntentHandler())
sb.add_request_handler(TurnOffIntentHandler())
sb.add_request_handler(OpenIntentHandler())
sb.add_request_handler(CloseIntentHandler())
sb.add_request_handler(InventoryIntentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())

sb.add_exception_handler(CatchAllExceptionHandler())

sb.add_global_request_interceptor(RequestLogger())
sb.add_global_response_interceptor(ResponseLogger())

lambda_handler = sb.lambda_handler()
