from lambda_local.main import call
from lambda_local.context import Context

import lambda_function
import json
import unittest
import logging
import sys
import copy

from alexa import util, objects, scene, data

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger('test_util')

def call_lambda_with_file(filepath):
    with open(filepath) as json_file:
        data = json.load(json_file)
        return call_lambda_with_json(data)

def call_lambda_with_json(json):
    context = Context(5)
    return call(lambda_function.lambda_handler, json, context)

class TestLambda(unittest.TestCase):

    def test_launch_returns_no_error(self):
        '''
        Triggering the launch intent should return a payload successfully and no errors
        '''
        (payload, error) = call_lambda_with_file('test/lambda_payloads/launch.json')
        self.assertIsNone(error)

    def test_launch_look_around_returns_description(self):
        '''
        Looking around the room should return the description of the room, and no error
        '''
        with open('test/lambda_payloads/look.json') as json_file:
            look_json = json.load(json_file)
        look_json["request"]["intent"]["name"] = "LookAroundIntent"

        (payload, error) = call_lambda_with_json(look_json)
        self.assertIn(data.DESCRIBE[objects.CLOSET], payload["response"]["outputSpeech"]["ssml"])

    def test_launch_look_at_window_description(self):
        '''
        Looking at the window should return the description of the room, and no error
        '''
        with open('test/lambda_payloads/one_object.json') as json_file:
            req_json = json.load(json_file)
        req_json["request"]["intent"]["name"] = "LookAtIntent"
        req_json["request"]["intent"]["slots"]["firstObject"]["value"] = objects.WINDOW

        (payload, error) = call_lambda_with_json(req_json)
        self.assertIn(data.LOOK_AT[objects.WINDOW], payload["response"]["outputSpeech"]["ssml"])

    def test_open_computer_description(self):
        '''
        Opening the computer should return the description for inside the computer
        '''
        with open('test/lambda_payloads/one_object.json') as json_file:
            req_json = json.load(json_file)
        req_json["request"]["intent"]["name"] = "OpenIntent"
        req_json["request"]["intent"]["slots"]["firstObject"]["value"] = objects.COMPUTER

        (payload, error) = call_lambda_with_json(req_json)
        self.assertIn(data.OPEN_RESPONSE[objects.COMPUTER], payload["response"]["outputSpeech"]["ssml"])

    def test_search_jacket_description(self):
        '''
        Searching the jacket should return the text description for finding the key
        '''
        with open('test/lambda_payloads/one_object.json') as json_file:
            req_json = json.load(json_file)
        req_json["request"]["intent"]["name"] = "SearchIntent"
        req_json["session"]["attributes"]["room"]["jacket"] = objects.STATE_FOUND
        req_json["request"]["intent"]["slots"]["firstObject"]["value"] = objects.JACKET

        (payload, error) = call_lambda_with_json(req_json)
        self.assertIn(data.SEARCH_RESPONSE[objects.JACKET], payload["response"]["outputSpeech"]["ssml"])
