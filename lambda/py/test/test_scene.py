import unittest
import logging
import sys
import json
import copy

from alexa import util, objects, scene, data

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger('test_util')

class TestScene(unittest.TestCase):

    def test_to_dict_has_six_items(self):
        '''
        The 'to_dict' function should give a dictionary with six keys
        '''
        room = scene.Scene()
        self.assertEqual(len(room.to_dict()), 7)

    def test_three_unnoticed_objects(self):
        '''
        Looking for all unnoticed objects should return four objects initially
        '''
        room = scene.Scene()
        self.assertEqual(len(room.get_objects_with_state(objects.STATE_UNNOTICED)), 4)

    def test_look_around_four_noticed_objects(self):
        '''
        Looking around the room should notice four objects and turn them to visible
        '''
        room = scene.Scene()
        room.look_around()
        self.assertEqual(len(room.get_objects_with_state(objects.STATE_VISIBLE)), 4)

    def test_look_at_non_existent_object_looks_around(self):
        '''
        Looking at an object that isn't there should trigger a look around
        '''
        room = scene.Scene()
        output = room.look_at('squirrel')
        self.assertIn(data.CANT_SEE_THAT, output)

    def test_look_at_computer_gives_object_description(self):
        '''
        Looking at an object that is there will trigger a look around if it's not visible yet
        '''
        room = scene.Scene()
        output = room.look_at(objects.COMPUTER)
        self.assertIn(data.WILL_LOOK_AROUND, output)

    def test_look_at_visible_computer_gives_just_object_description(self):
        '''
        Looking at an object that is visible should just give the object description
        '''
        room = scene.Scene()
        room.look_around()
        output = room.look_at(objects.COMPUTER)
        self.assertEqual(data.LOOK_AT[objects.COMPUTER], output)

    def test_look_at_hidden_object_doesnt_find_it(self):
        '''
        Looking at an object that is hidden should try to find it, but not actually find it
        '''
        room = scene.Scene()
        room.look_around()
        output = room.look_at(objects.USB_KEY)
        self.assertNotIn(data.LOOK_AT[objects.USB_KEY], output)

    def test_look_at_window_reveals_window(self):
        '''
        If objects have become visible for a scene, then dehydrating/rehydrating it
        should maintain that state
        '''
        room = scene.Scene()
        output = room.look_at(objects.WINDOW)
        self.assertIn(data.LOOK_AT[objects.WINDOW], output)

    def test_can_open_computer(self):
        '''
        Computer should be an object that can be opened according to can_perform_action
        '''
        room = scene.Scene()
        self.assertTrue(room.can_perform_action(objects.COMPUTER, objects.ACTION_OPEN))

    def test_cant_open_window(self):
        '''
        Window should be an object that can't be opened according to can_perform_action
        '''
        room = scene.Scene()
        self.assertFalse(room.can_perform_action(objects.WINDOW, objects.ACTION_OPEN))

    def test_opening_computer_changes_state(self):
        '''
        Opening the computer should change the object state
        '''
        room = scene.Scene()
        room.look_at(objects.COMPUTER)
        output = room.open(objects.COMPUTER)
        self.assertIn(data.OPEN_RESPONSE[objects.COMPUTER], output)
        self.assertEqual(room.room_objects[objects.COMPUTER].current_state.identifier, objects.STATE_OPENED)

    def test_cant_open_computer_twice(self):
        '''
        Once the computer is open, shouldn't be able to open it AGAIN
        '''
        room = scene.Scene()
        room.look_at(objects.COMPUTER)
        room.open(objects.COMPUTER)
        output = room.open(objects.COMPUTER)
        self.assertIn(data.OPEN_RESPONSE[data.ALREADY_KEY], output)

    def test_cant_open_computer_without_noticing(self):
        '''
        Should not be able to open an object without noticing it first
        '''
        room = scene.Scene()
        output = room.open(objects.COMPUTER)
        self.assertIn(data.OBJECT_UNNOTICED, output)

    def test_opening_closet_notices_jacket(self):
        '''
        Opening the closet should notice the jacket in the closet
        '''
        room = scene.Scene()
        room.look_around() # notice all objects clearly in sight
        output = room.open(objects.CLOSET)
        self.assertIn(data.OPEN_RESPONSE[objects.CLOSET], output)
        self.assertEqual(room.room_objects[objects.JACKET].current_state.identifier, objects.STATE_FOUND)

    def test_noticing_jacket_adds_to_room_description(self):
        '''
        Once the jacket has been noticed, it should be added to the room description
        '''
        room = scene.Scene()
        room.look_around() # notice all objects clearly in sight
        room.open(objects.CLOSET)
        output = room.look_around()
        self.assertIn(data.DESCRIBE[objects.JACKET], output)

    def test_searching_jacket_gives_search_description(self):
        '''
        Once the jacket has been searched, the key should be visible
        '''
        room = scene.Scene()
        room.look_around() # notice all objects clearly in sight
        room.open(objects.CLOSET)
        output = room.search(objects.JACKET)
        self.assertIn(data.SEARCH_RESPONSE[objects.JACKET], output)

    def test_searching_jacket_adds_key_to_room_description(self):
        '''
        Once the jacket has been searched, the key should be visible
        '''
        room = scene.Scene()
        room.look_around() # notice all objects clearly in sight
        room.open(objects.CLOSET)
        room.search(objects.JACKET)
        output = room.look_around()
        self.assertIn(data.DESCRIBE[objects.KEY], output)

    def test_searching_fiing_cabinet_adds_usb_key_to_room_description(self):
        '''
        Once the filing cabinet has been searched, the usb key should be visible
        '''
        room = scene.Scene()
        room.look_around() # notice all objects clearly in sight
        room.open(objects.FILING_CABINET)
        room.search(objects.FILING_CABINET)
        output = room.look_around()
        self.assertIn(data.DESCRIBE[objects.USB_KEY], output)

    def test_cant_search_twice(self):
        '''
        Once the jacket has been searched, can't search it again
        '''
        room = scene.Scene()
        room.look_around() # notice all objects clearly in sight
        room.open(objects.CLOSET)
        room.search(objects.JACKET)
        output = room.search(objects.JACKET)
        self.assertIn(data.SEARCH_RESPONSE[data.ALREADY_KEY], output)
