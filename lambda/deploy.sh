#!/bin/sh
function timestamp {
  echo $1 `date "+%H:%M:%S"` $2
}

if [ -d skill_env ];
then
    rm -rf skill_env
fi

mkdir skill_env

# We only do this if we're not using layers correctly
#timestamp "PIP" "START"
#pip install -r requirements.txt -t skill_env --upgrade
#timestamp "PIP" "END"

timestamp "ZIP" "START"
cd py
zip -r ../pointandclick-lambda.zip * -x 'test/*' -x '*/__pycache__/*'
cd ..
timestamp "ZIP" "END"

timestamp "DEPLOY" "START"
aws lambda update-function-code --function-name ask-custom-point-and-click --zip-file fileb://pointandclick-lambda.zip
timestamp "DEPLOY" "END"
rm pointandclick-lambda.zip
